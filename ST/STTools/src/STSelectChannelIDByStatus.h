#ifndef STSelectChannelIDByStatus_H
#define STSelectChannelIDByStatus_H 1

#include <string>
#include <vector>

// ST tool base class
#include "Kernel/STToolBase.h"

// LHCbKernel
#include "Kernel/ISTChannelIDSelector.h"

#include "STDet/DeSTSector.h"

/** @class STSelectChannelIDByStatus STSelectChannelIDByStatus.h
 *
 *  Tool for selecting clusters using the conditions
 *
 *  @author M.Needham
 *  @date   3/2/2009
 */

class STSelectChannelIDByStatus: public ST::ToolBase, 
                         virtual public ISTChannelIDSelector {

 public: 
   
  /// constructer
  STSelectChannelIDByStatus( const std::string& type,
                         const std::string& name,
                         const IInterface* parent );

  /// intialize
  StatusCode initialize() override;

  /**  @param  cluster pointer to ST cluster object to be selected 
  *  @return true if cluster is selected
  */
  bool select     ( const LHCb::STChannelID& id ) const override;
  
  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to ST cluster object to be selected 
   *  @return true if cluster is selected
   */
  bool operator() ( const LHCb::STChannelID& id ) const override;


private:

  std::vector<std::string> m_statusNames;
  std::vector<DeSTSector::Status> m_statusList;

  STSelectChannelIDByStatus() = delete;
  STSelectChannelIDByStatus(const STSelectChannelIDByStatus& ) = delete;
  STSelectChannelIDByStatus& operator= (const STSelectChannelIDByStatus& ) = delete; 
 
};

#endif // STSelectChannelIDByStatus_H
