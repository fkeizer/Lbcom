#ifndef STSelectChannelIDByElement_H
#define STSelectChannelIDByElement_H 1

// ST tool base class
#include "Kernel/STToolBase.h"

// LHCbKernel
#include "Kernel/ISTChannelIDSelector.h"

/** @class STSelectChannelIDByElement STSelectChannelIDByElement.h
 *
 *  Tool for selecting clusters using the conditions
 *
 *  @author M.Needham
 *  @date   3/2/2009
 */

class DeSTBaseElement;

class STSelectChannelIDByElement: public ST::ToolBase, 
                         virtual public ISTChannelIDSelector {

 public: 
   
  /// constructer
  STSelectChannelIDByElement( const std::string& type,
                         const std::string& name,
                         const IInterface* parent );

  /// intialize 
  StatusCode initialize() override;

  /**  @param  cluster pointer to ST cluster object to be selected 
  *  @return true if cluster is selected
  */
  bool select     ( const LHCb::STChannelID& id ) const override;
  
  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to ST cluster object to be selected 
   *  @return true if cluster is selected
   */
  bool operator() ( const LHCb::STChannelID& id ) const override;


private:

  ///   default  constructor  is  private 
  STSelectChannelIDByElement();

  ///   copy     constructor  is  private 
  STSelectChannelIDByElement(const STSelectChannelIDByElement& );

  ///   assignement operator  is  private 
  STSelectChannelIDByElement& operator= (const STSelectChannelIDByElement& );  
 
  std::vector<DeSTBaseElement*> m_detElements;

  std::vector<std::string> m_elementNames;

};

#endif // STSelectChannelIDByElement_H
