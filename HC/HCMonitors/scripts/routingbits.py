def bits(mask):
  result = []
  words = mask.split(',')
  nWords = len(words)
  for i in range(nWords):
    word = int(words[i], 16)
    for j in range(32):
      if (1 << j) & word:
        result.append(i * 32 + j)
  return result 
 
def mask(bits):
  words = [0, 0, 0, 0]
  for bit in bits:
    i = bit / 32
    words[i] = words[i] | (1 << (bit - 32 * i))
  result = ""
  for i in range(4):
    result += format(words[i], "#x")
    if i < 3: result += ","
  return result

triggerBits = [48, 57, 96]
vetoBits = [35, 104, 105, 106]
triggerMask = mask(triggerBits)
vetoMask = mask(vetoBits)
print "TriggerMask=" + triggerMask + ";VetoMask=" + vetoMask

print bits("0x0,0x2010000,0x0,0x1")
print bits("0x0,0x0,0x0,0x700")
