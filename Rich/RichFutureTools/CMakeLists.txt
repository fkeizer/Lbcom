################################################################################
# Package: RichFutureTools
################################################################################
gaudi_subdir(RichFutureTools v1r0)

gaudi_depends_on_subdirs(Det/RichDet
                         Kernel/PartProp
                         Rich/RichFutureKernel
                         Rich/RichUtils
                         Kernel/VectorClass)

find_package(Vc)
find_package(Boost)
find_package(ROOT)

#message(STATUS "Using Vc ${Vc_INCLUDE_DIR} ${Vc_LIB_DIR}")
#message(STATUS "Vc DEFINITIONS ${Vc_DEFINITIONS}")
#message(STATUS "Vc COMPILE flags ${Vc_COMPILE_FLAGS}")
#message(STATUS "Vc ARCHITECTURE flags ${Vc_ARCHITECTURE_FLAGS}")

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${Vc_INCLUDE_DIR})

gaudi_add_module(RichFutureTools
                 src/*.cpp
                 INCLUDE_DIRS Vc Det/RichDet Rich/RichFutureKernel Kernel/VectorClass Rich/RichUtils
                 LINK_LIBRARIES RichDetLib PartPropLib RichFutureKernel RichUtils VectorClassLib)

# Fixes for GCC7.
if( BINARY_TAG_COMP_NAME STREQUAL "gcc" AND BINARY_TAG_COMP_VERSION VERSION_GREATER "6.99")
  set_property(TARGET RichFutureTools APPEND PROPERTY COMPILE_FLAGS " -faligned-new ")
endif()
