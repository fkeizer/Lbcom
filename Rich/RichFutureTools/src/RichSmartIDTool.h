
//-----------------------------------------------------------------------------
/** @file RichSmartIDTool.h
 *
 *  Header file for tool : Rich::SmartIDTool
 *
 *  @author Antonis Papanestis
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2003-10-28
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <vector>
#include <array>

// Base class
#include "RichFutureKernel/RichToolBase.h"

// Interfaces
#include "RichInterfaces/IRichSmartIDTool.h"
#include "RichInterfaces/IRichDetectorTool.h"

// Utils
#include "RichUtils/RichSmartIDSorter.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichMap.h"

// RichDet
#include "RichDet/DeRich.h"
#include "RichDet/DeRichPDPanel.h"
#include "RichDet/DeRichSystem.h"

namespace Rich
{
  namespace Future
  {

    //-----------------------------------------------------------------------------
    /** @class SmartIDTool RichSmartIDTool.h
     *
     *  A tool to preform the manipulation of RichSmartID channel identifiers
     *
     *  @author Antonis Papanestis
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   2003-10-28
     *
     *  @todo Move application of panel offset in RichSmartIDTool::globalToPDPanel
     *        into DeRichPDPanel class
     */
    //-----------------------------------------------------------------------------

    class SmartIDTool final : public ToolBase,
                              virtual public ISmartIDTool
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      SmartIDTool( const std::string& type,
                   const std::string& name,
                   const IInterface* parent );

      // Initialization of the tool after creation
      virtual StatusCode initialize() override;

    public: // methods (and doxygen comments) inherited from interface

      // Converts a RichSmartID channel identification into a position in global LHCb coordinates.
      virtual bool globalPosition ( const LHCb::RichSmartID& smartid,
                                    Gaudi::XYZPoint& detectPoint ) const override;

      // Finds the average position of a cluster of RichSmartIDs, in global LHCb coordinates
      virtual bool globalPosition ( const Rich::PDPixelCluster& cluster,
                                    Gaudi::XYZPoint& detectPoint ) const override;

      // Converts an PD RichSmartID identification into a position in global LHCb coordinates.
      virtual bool pdPosition ( const LHCb::RichSmartID& pdid,
                                Gaudi::XYZPoint& pdPoint ) const override;

      // Computes the global position coordinate for a given position in local
      virtual Gaudi::XYZPoint globalPosition ( const Gaudi::XYZPoint& localPoint,
                                               const Rich::DetectorType rich,
                                               const Rich::Side side ) const override;

      // Converts a position in global coordinates to the corresponding RichSmartID
      virtual bool smartID( const Gaudi::XYZPoint& globalPoint,
                            LHCb::RichSmartID& smartid ) const override;

      // Supplies a vector of all currently active and valid channels in the RICH detectors
      virtual LHCb::RichSmartID::Vector readoutChannelList( ) const override;

      // Converts a position in global coordinates to the local coordinate system.
      virtual Gaudi::XYZPoint globalToPDPanel ( const Gaudi::XYZPoint& globalPoint ) const override;

    private:

      /// photodetector panels per rich
      using PDPanelsPerRich = PanelArray<const DeRichPDPanel*>;

      /// typedef for photodetector for each rich
      using RichPDPanels = DetectorArray<PDPanelsPerRich>;

    private:

      /// Get the PD panel for the given SmartID
      inline const DeRichPDPanel* panel( const LHCb::RichSmartID& ID ) const noexcept
      {
        return (m_photoDetPanels[ID.rich()])[ID.panel()];
      }

      /// Get the PD panel for the given RICH and side
      inline const DeRichPDPanel* panel( const Rich::DetectorType rich,
                                         const Rich::Side side ) const noexcept
      {
        return (m_photoDetPanels[rich])[side];
      }
      
      /// Get the position for a given SmartID.
      inline bool _globalPosition( const LHCb::RichSmartID& smartID,
                                   Gaudi::XYZPoint& detectPoint ) const
      {
        return panel(smartID)->detectionPoint(smartID,detectPoint,m_hitPhotoCathSide);
      }
      
    private:

      /// photodetector for each rich
      RichPDPanels m_photoDetPanels;

      /// RichSystem object
      DeRichSystem* m_richS = nullptr;

      /// false to get the hit on the outside of PD window (inlcuding refraction)
      Gaudi::Property<bool> m_hitPhotoCathSide { this, "HitOnPhotoCathSide", false };

      /// Pointer to detector tool for DeRich objects
      ToolHandle<const IDetectorTool> m_deRichTool { "Rich::Future::DetectorTool/RichDetectorTool", this };

    };

  }
}
