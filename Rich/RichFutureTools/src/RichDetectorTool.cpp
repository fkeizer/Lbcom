
//-----------------------------------------------------------------------------
/** @file RichDetectorTool.cpp
 *
 * Implementation file for class : Rich::DetectorTool
 *
 * @author Antonis Papanestis
 * @date 2012-10-26
 */
//-----------------------------------------------------------------------------

// local
#include "RichDetectorTool.h"

using namespace Rich::Future;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DetectorTool::DetectorTool( const std::string& type,
                            const std::string& name,
                            const IInterface* parent )
  : ToolBase ( type, name, parent )
{
  // Interface
  declareInterface<IDetectorTool>(this);
}

//=========================================================================
//  deRichDetectors
//=========================================================================
std::vector<DeRich*> DetectorTool::deRichDetectors( ) const
{
  // find the DeRich objects
  std::vector<std::string> locations;

  auto afterMagnet = getDet<DetectorElement>("/dd/Structure/LHCb/AfterMagnetRegion");
  if ( afterMagnet->exists("RichDetectorLocations") )
  {
    locations = afterMagnet->paramVect<std::string>("RichDetectorLocations");
  }
  else
  {
    locations = { DeRichLocations::Rich1,
                  DeRichLocations::Rich2 };
  }

  std::vector<DeRich*> deRichDets;
  deRichDets.reserve( locations.size() );

  for ( const auto & loc : locations )
  {
    deRichDets.push_back( getDet<DeRich>(loc) );
  }

  return deRichDets;
}

//=========================================================================

DECLARE_COMPONENT( DetectorTool )

//=========================================================================
