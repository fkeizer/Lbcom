################################################################################
# Package: VPTools
################################################################################
gaudi_subdir(VPTools v2r6)

gaudi_depends_on_subdirs(Det/VPDet
                         Event/DigiEvent
                         Event/TrackEvent
                         GaudiAlg
                         Tr/LHCbTrackInterfaces)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(VPTools
                 src/*.cpp
                 INCLUDE_DIRS Event/DigiEvent Tr/LHCbTrackInterfaces
                 LINK_LIBRARIES VPDetLib TrackEvent GaudiAlgLib)

